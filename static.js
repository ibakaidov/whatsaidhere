const getConnection = require('./db');


module.exports = ({user, type})=>{
    return new Promise((resolve, reject)=>{
      let db = getConnection();
      db.connect();
      db.query('INSERT INTO messages SET ?', {uid:user, type, date:+new Date()}, (err, result)=>{
        db.end();
        if (err) return reject(err);
        resolve();
      })
    })
};