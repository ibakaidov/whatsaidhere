const apiai = require('apiai');
const uniqid = 'e4278f61-9437-4dff-a24b-6c4bb2995611';

const app = apiai("408a6430cd284485906a31d34f442bc8");

module.exports = (q, uid) => {

  return new Promise((resolve, reject) => {
    uid = uid + ''
    let cuniqid = uniqid.slice(0, uniqid.length - uid.length) + uid

    const request = app.textRequest(q, {
      sessionId: uniqid
    });

    request.on('response', (response) => {
      resolve(response);
    });

    request.on('error', (error) => {
      reject(error);
    });

    request.end();

  })
}
