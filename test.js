const api = new require('api.vk.com');
const request = require('request');
const Bot = require('node-vk-bot-api')

const asr = require('./asr')

const invite = require('./invite')

const token = 'eca1ea91534904c9dda53d11774abc41a5ff530814807dca7e4f28209f0e1af40e41162f206c5055c3b05';



const bot = new Bot({ token })

bot.command('attach', (ctx) => {
    console.log(ctx)
})

bot.on((ctx) => {
    let object = ctx;
    let url = null;
    new Promise((resolve, reject) => {
        if (object.attachments.length != 0) {
            api('messages.getById', { access_token: token, message_ids: object.message_id, v: 5.67 }, (err, res) => {
                if (err) return reject(err);
                let msg = res.items[0];
                if (msg.attachments[0].type != 'doc') return reject('Вы прикрепили не звуковое сообщение');
                if (msg.attachments[0].doc.type!=5) return reject('Вы прикрепили не звуковое сообщение');
                
                resolve(res.items[0].attachments[0].doc.preview.audio_msg.link_mp3)
            });


        } else if (object.forward != null) {
            resolve(object.forward.attachments[0].doc.preview.audio_msg.link_mp3)

        }else {
            resolve(null)
        }
    }).then((url) => {
        return new Promise((resolve, reject) => {

            if (url == null) {
                return reject('Мы не нашли голосовых сообщений. Отправьте нам свой голос или перешлите его')
            }

            request.get({ url, encoding: null }, (err, r, body) => {
                ctx.reply('Начинаю распознавать голос')

                asr(body).then((phrases) => {
                    if (phrases.variant != null && phrases.variant[0] != null && phrases.variant[0]._) {
                        ctx.reply(phrases.variant[0]._, (err, mesid) => {
                            
                        });
                        invite(ctx.user_id)                            
                        
                    } else {
                        reject('Не удалось распознать')
                    }
                }).catch((error) => {
                    reject("сбой в распознавании речи")
                })
            })
        })
    }).catch((error) => {
        ctx.reply(error)
    });



})

bot.listen()

