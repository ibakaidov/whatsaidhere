const mysql = require('mysql');

const settings = {
    host: 'localhost',
    user: 'root',
    password: 'lbfyf2007',
    database: 'wsh'
};

module.exports = getConnection;

createTables();

function createTables() {
    let db = getConnection();

    db.connect();

    db.query('CREATE TABLE IF NOT EXISTS messages (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, uid INT, type INT, date LONG)', function (error, results, fields) {
        if (error) throw error;
        console.log('The solution is: ', results);
    });

    db.end();
};

function getConnection() {
    return mysql.createConnection(settings);
};