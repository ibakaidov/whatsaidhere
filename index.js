const vk = require('api.vk.com')
const request = require('request-promise')
const Bot = require('node-vk-bot-api')


const asr = require('./asr')
const invite = require('./invite')
const static = require('./static')
const assistent = require('./assistent')

const token = require('./token');

const api = (method, options) => {
    return new Promise((resolve, reject) => {
        vk(method, options, (err, result) => {
            if (err) return reject(err)
            resolve(result)
        })
    })
}

const bot = new Bot({ token })

bot.on(async (ctx) => {
    let object = ctx;
    let uri = null;

    try {
        api('messages.setActivity', { access_token: token, type: 'typing', user_id: object.user_id })

        api('messages.markAsRead', { access_token: token, message_ids: object.message_id })

        let uri
        if (object.attachments.length != 0) {
            const [msg] = (await api('messages.getById', { access_token: token, message_ids: object.message_id, v: 5.67 })).items
            console.log(msg.attachments)
            if ((msg.attachments[0].type != 'doc' || msg.attachments[0].type != 'audio') || msg.attachments[0].doc.type != 5) {
                static({ user: object.user_id, type: 0 });

                uri = null
            }

            try {
                if (msg.attachments[0].type === 'doc') uri = msg.attachments[0].doc.preview.audio_msg.link_mp3
                else if (msg.attachments[0].type === 'audio') uri = msg.attachments[0].audio.url
            } catch (e) {
                uri = null
            }


        } else if (object.forward != null) {
            try {
                uri = (object.forward.attachments[0].doc.preview.audio_msg.link_mp3)
            }
            catch (e) {

                uri = (null)
            }

        } else {
            uri = (null)
        }

        if (uri === '') {
            throw ('Не могу послушать это аудио')
        }

        if (uri == null) {
            static({ user: object.user_id, type: 0 });
            if (object.body != '') {
                const { speech } = (await assistent(object.body, object.user_id)).result.fulfillment
                ctx.reply(speech)
                return
            }
            else {
                throw ('Мы не нашли голосовых сообщений. Отправь нам свой голос или перешлите его')
            }

        }

        const audio = await request.get({ uri, encoding: null })
        let phrase
        try {
            phrase = await asr(audio)
        }
        catch (e) {
            static({ user: object.user_id, type: 2 });
            throw ("сбой в распознавании речи")

        }
        if (phrase != null) {
            ctx.reply(phrase, (err, mesid) => {

            });
            static({ user: object.user_id, type: 3 });
            invite(ctx.user_id)
        } else {
            static({ user: object.user_id, type: 1 });

            throw ('Не удалось распознать')
        }

    } catch (error) {
        console.error(error);
        ctx.reply('string' == typeof error ? error : 'Произошла непонятная ошибка. Я не знаю, что делать!')
    };



})

bot.listen()

